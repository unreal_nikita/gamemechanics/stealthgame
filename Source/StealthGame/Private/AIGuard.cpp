// Fill out your copyright notice in the Description page of Project Settings.


#include "AIGuard.h"

#include "DrawDebugHelpers.h"
#include "NavigationSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "../StealthGameGameMode.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Perception/PawnSensingComponent.h"

#include <concrt.h>

// Sets default values
AAIGuard::AAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Sensing Coomponent"));
	PawnSensingComponent->OnSeePawn.AddDynamic(this,&AAIGuard::OnPawnSee);
	PawnSensingComponent->OnHearNoise.AddDynamic(this, &AAIGuard::AAIGuard::OnNoiseHeard);

	GuardState = EAIState::Idel;
}

// Called when the game starts or when spawned
void AAIGuard::BeginPlay()
{
	Super::BeginPlay();
	OriginalRotaion = GetActorRotation();

	if (bIsPatrol)
	{
		MoveToNextPatrolPoint();
	}
}

void AAIGuard::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}

	// this->SetActorLocation(CurrentPatrolPoint->GetActorLocation());
	UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}

void AAIGuard::ResetOrientaion()
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}
	SetActorRotation(OriginalRotaion);

	SetGuardState(EAIState::Idel);

	if (bIsPatrol)
	{
		MoveToNextPatrolPoint();
	}
}

void AAIGuard::OnPawnSee(APawn* SeenPawn)
{
	if (SeenPawn)
	{
		DrawDebugSphere(GetWorld(), SeenPawn->GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);
	}

	AStealthGameGameMode* MyGameMode = Cast<AStealthGameGameMode>(GetWorld()->GetAuthGameMode());
	if (MyGameMode)
	{
		MyGameMode->CompleteMission(SeenPawn, false);
	}

	SetGuardState(EAIState::Alerted);

	AController* AIController = GetController();
	if (AIController)
	{
		AIController->StopMovement();
	}
}

void AAIGuard::OnNoiseHeard(APawn* NoiseInstigator, const FVector& NoiseLocation, float Volume)
{
	if (GuardState == EAIState::Alerted)
	{
		return;
	}
	DrawDebugSphere(GetWorld(), NoiseLocation, 32, 12, FColor::Green, false, 10.0f);

	FVector Direction = NoiseLocation - GetActorLocation();
	Direction.Normalize();
	FRotator AILookAt = FRotationMatrix::MakeFromX(Direction).Rotator();

	AILookAt.Pitch = 0.0f;
	AILookAt.Roll = 0.0f;

	SetActorRotation(AILookAt);

	GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientaion);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientaion, this, &AAIGuard::ResetOrientaion, 3.0f, false);

	SetGuardState(EAIState::Suspicious);

	AController* AIController = GetController();
	if (AIController)
	{
		AIController->StopMovement();
	}
}

void AAIGuard::SetGuardState(EAIState State)
{
	if (GuardState == State)
	{
		return;
	}

	GuardState = State;
	OnStateChanged(GuardState);
}

// Called every frame
void AAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPatrolPoint)
	{
		FVector DelatDistace = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		float DistaceToGoal = DelatDistace.Size();

		if (DistaceToGoal < 50)
		{
			MoveToNextPatrolPoint();
		}
	}

}

