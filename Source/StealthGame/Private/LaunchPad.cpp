// Fill out your copyright notice in the Description page of Project Settings.


#include "LaunchPad.h"

#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "StealthGame/StealthGameCharacter.h"

// Sets default values
ALaunchPad::ALaunchPad()
{
	OverlapComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Overlap component"));
	OverlapComponent->SetBoxExtent(FVector(200.0f, 200.0f, 50.0f));
	OverlapComponent->SetHiddenInGame(true);
	RootComponent = OverlapComponent;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	MeshComponent->SetupAttachment(RootComponent);

	OverlapComponent->OnComponentBeginOverlap.AddDynamic(this, &ALaunchPad::OveralapLaunchPad);

	LaunchStrength = 1500.0f;
	LaunchPitchAngle = 35.0f;
}

void ALaunchPad::OveralapLaunchPad(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FRotator LaunchDirection = GetActorRotation();
	LaunchDirection.Pitch += LaunchAngle;
	FVector LaunchVelocity = LaunchDirection.Vector() * LaunchStrength;

	AStealthGameCharacter* MyCharacter = Cast<AStealthGameCharacter>(OtherActor);
	if (MyCharacter)
	{
		PlayFX(LaunchPadFX);
		MyCharacter->LaunchCharacter(LaunchVelocity, true, true);
	}
	else if (OtherComp && OtherComp->IsSimulatingPhysics())
	{
		PlayFX(LaunchPadFX);
		OtherComp->AddImpulse(LaunchVelocity, NAME_None, true);
	}
}

void ALaunchPad::PlayFX(UParticleSystem* InitFX)
{
	UGameplayStatics::SpawnEmitterAtLocation(this, InitFX, GetActorLocation());
}



