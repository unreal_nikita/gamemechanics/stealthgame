// Fill out your copyright notice in the Description page of Project Settings.


#include "ExtractionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "StealthGame/StealthGameCharacter.h"
#include "StealthGame/StealthGameGameMode.h"

// Sets default values
AExtractionZone::AExtractionZone()
{
	OverlapComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Overlap component"));
	OverlapComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	OverlapComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapComponent->SetBoxExtent(FVector(200.0f));
	OverlapComponent->SetHiddenInGame(true);
	RootComponent = OverlapComponent;

	OverlapComponent->OnComponentBeginOverlap.AddDynamic(this, &AExtractionZone::HandleOverlap);

	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal Component"));
	DecalComponent->DecalSize = FVector(200.0f, 200.0f, 200.0f);
	DecalComponent->SetupAttachment(RootComponent);
}

void AExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AStealthGameCharacter* MyCharacter = Cast<AStealthGameCharacter>(OtherActor);
	if (MyCharacter == nullptr)
	{
		return;
	}

	if (MyCharacter->bIsCarryingObjective == true)
	{
		AStealthGameGameMode* MyGameMode = Cast<AStealthGameGameMode>(GetWorld()->GetAuthGameMode());
		if (MyGameMode)
		{
			MyGameMode->CompleteMission(MyCharacter, true);
		}
	}
	else
	{
		UGameplayStatics::PlaySound2D(this, SoundMissingObjective);
	}

	UE_LOG(LogTemp, Log, TEXT("Overlapped with extraction zone!"));
}

