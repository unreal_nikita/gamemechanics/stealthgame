// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "LaunchPad.generated.h"

UCLASS()
class STEALTHGAME_API ALaunchPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALaunchPad();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Mesh Component")
	class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(VisibleAnywhere, Category = "Overlap Component")
	class UBoxComponent* OverlapComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* LaunchPadFX;

	UPROPERTY(EditInstanceOnly, Category = "Launch Strength")
	float LaunchStrength;

	UPROPERTY(EditInstanceOnly, Category = "Launch Angle")
	float LaunchAngle;

	UPROPERTY(EditInstanceOnly, Category = "Launch Pitch Angle")
	float LaunchPitchAngle;

public:

	void PlayFX(UParticleSystem* InitFX);

	UFUNCTION()
	void OveralapLaunchPad(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
