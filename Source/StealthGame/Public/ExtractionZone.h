// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "ExtractionZone.generated.h"

UCLASS()
class STEALTHGAME_API AExtractionZone : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = "Component")
	UBoxComponent* OverlapComponent;

	UPROPERTY(VisibleAnywhere, Category = "Component")
	UDecalComponent* DecalComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundBase* SoundMissingObjective = nullptr;
public:	
	// Sets default values for this actor's properties
	AExtractionZone();

protected:

	UFUNCTION()
	void HandleOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
