// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/TargetPoint.h"
#include "GameFramework/Character.h"
#include "Perception/PawnSensingComponent.h"

#include "AIGuard.generated.h"

UENUM(BlueprintType)
enum class EAIState : uint8
{
	Idel,
	Suspicious,
	Alerted
};

UCLASS()
class STEALTHGAME_API AAIGuard : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = "Guard Component")
	class UPawnSensingComponent* PawnSensingComponent;

public:
	// Sets default values for this character's properties
	AAIGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditInstanceOnly, Category = "Patrol")
	bool bIsPatrol = true;

	UPROPERTY(EditInstanceOnly, Category = "Patrol Point", meta = (EditCondition = "bIsPatrol"))
	AActor* FirstPatrolPoint;

	UPROPERTY(EditInstanceOnly, Category = "Patrol Point", meta = (EditCondition = "bIsPatrol"))
	AActor* SecondPatrolPoint;

	AActor* CurrentPatrolPoint;

	FRotator OriginalRotaion;
	FTimerHandle TimerHandle_ResetOrientaion;

	UFUNCTION()
	void MoveToNextPatrolPoint();

	UFUNCTION()
	void ResetOrientaion();

	UFUNCTION()
	void OnPawnSee(APawn* SeenPawn);

	UFUNCTION()
	void OnNoiseHeard(APawn* NoiseInstigator, const FVector& NoiseLocation, float Volume);

	EAIState GuardState;

	UFUNCTION()
	void SetGuardState(EAIState State);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnStateChanged(EAIState NewState);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
