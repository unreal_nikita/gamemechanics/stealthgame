// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "StealthGameObjective.generated.h"

UCLASS()
class STEALTHGAME_API AStealthGameObjective : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = "Component")
	class UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Component")
	class USphereComponent* SphereComp;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* PickupFX;

public:	
	// Sets default values for this actor's properties
	AStealthGameObjective();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void PlayEffects();

public:	

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
