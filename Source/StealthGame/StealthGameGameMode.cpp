// Copyright Epic Games, Inc. All Rights Reserved.

#include "StealthGameGameMode.h"
#include "StealthGameHUD.h"
#include "StealthGameCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

AStealthGameGameMode::AStealthGameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
		TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AStealthGameHUD::StaticClass();
}

void AStealthGameGameMode::CompleteMission(APawn* InstigatorPawn, bool bIsMissionSuccess)
{
	if (InstigatorPawn)
	{
		InstigatorPawn->DisableInput(nullptr);

		if (SpectatingViewopointClass)
		{
			TArray<AActor*> ReturnedActors;
			UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewopointClass, ReturnedActors);

			// Change view target if find valid actor in TArray
			AActor* NewPointView = nullptr;
			if (ReturnedActors.Num() > 0)
			{
				NewPointView = ReturnedActors[0];

				APlayerController* MyPlayerController = Cast<APlayerController>(InstigatorPawn->GetController());
				if (MyPlayerController)
				{
					MyPlayerController->SetViewTargetWithBlend(NewPointView, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
				}
			}
		}
		UE_LOG(LogTemp, Warning, TEXT("SpectatingViewopointClass is nullptr. pls updating value"))
	}
	OnMissionCompleted(InstigatorPawn, bIsMissionSuccess);
}
